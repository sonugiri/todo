import React, { Component } from 'react';
class Item extends Component {
  constructor(props) {
    super(props)
    this.state = {
      completed: this.props.user.complete
    }
    
  }
  deleteHandler = (id) => {
    this.props.deleteItem(id);
  }

  completeHandler = (id) => {
    this.setState({
      completed : !this.state.completed
    });
    this.props.completeItem(id);
  }

  render() {
    let listyle = { padding: '10px', height: "30px", width: "100px", display: "inline-block", borderBottom: "1px solid #ccc" }
    let chk = this.props.user.complete ?
      <button type="button" className="btn btn-primary">Undo</button> :
      <button type="button" className="btn btn-primary">Done</button>;

    return (

      <li className={this.state.completed ? 'text-strike' : null} key={this.props.user.id} style={{ listStyleType: "none" }}>
        <span style={listyle}>{this.props.user.id}</span>
        <span style={listyle}>{this.props.user.name}</span>
        <span style={listyle}>{this.props.user.age}</span>
        <span onClick={() => (this.deleteHandler(this.props.user.id))} style={{ padding: '10px', cursor: "pointer", height: "30px", width: "100px", display: "inline-block", color: "red", borderBottom: "1px solid #ccc" }}>X</span>
        <span
          onClick={() => (this.completeHandler(this.props.user.id))}
          style={{ padding: '10px', cursor: "pointer", height: "30px", width: "100px", display: "inline-block", color: "green" }}>
          {chk}
        </span>

      </li>
    );
  }
}

export default Item;