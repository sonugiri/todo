import React, { Component } from 'react';

class Additem extends Component {
    constructor(props) {
        super(props);

        this.state= {
            item : '',
            err : ''
        };
    }
    handleChange = (event) => {
        this.setState({
            item: event.target.value 
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (!this.state.item) {
            this.setState({
                err : 'Please enter item'
            })
            return false;
        } else {
            this.setState({
                err : ''
            })
        }
        this.props.addTodo(this.state.item);
        this.setState({
            item: ''
        });
    }

    

    render() {
        return (
            <div>
                {
                this.state.err ? <div className="alert alert-danger">
                <strong>Alert!</strong> { this.state.err}
                </div> : ''
            }
            <form className="form-inline" onSubmit={this.handleSubmit}>
            
                <div className="form-group col-6">
                    <label htmlFor="email">Add:</label>
                    <input type="text" 
                    className="form-control"
                    value={ this.state.item }
                    onChange= { this.handleChange }
                    />
                </div>
                <button type="submit" className="btn btn-primary">Add</button>
            </form>
            </div>
        );
    }
}

export default Additem;