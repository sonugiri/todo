import React, { Component } from 'react';

import './App.css';

import customData from './customData';
import Additem from './Additem';
import Item from './Item';




class List extends Component {
  deleteItem = (id)  => {
    this.props.deleteItem(id);
  }
  completeItem = (id) => {
    this.props.completeItem(id);
  }
  render() {
    let userList = this.props.users;
    return (
      <div>
        <ul style={{ width: "100%"}}>
        
        {
          userList.map((user) => (
            <Item user = {user} completeItem = {this.completeItem} deleteItem={ this.deleteItem} />
          ))
        }
        </ul>
      </div>
    );
  }
}


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items : customData
    }
  }

  addTodo = (val) => {
    let prevState = this.state.items;
    let maxId = Math.max.apply(Math, prevState.map(function(o) { return (isNaN(o.id) ? 1 : Number(o.id) + 1 ); }))
   
    let newState = [...prevState,
    {
      id: maxId,
      name: val,
      age: (Math.floor(Math.random() * (30 - 20) + 20)),
      complete: false 
   }];
   this.setState({
     items: newState
   });
}

deleteItem = (id) => {
  
  let newState = this.state.items.filter((item) => (item.id != id));
  this.setState({
    items : newState
  })
}

completeItem = (id) => {
  let newState = [...this.state.items];
  let upState = newState.map((item) => {
    if (item.id == id) {
      item.complete = !item.complete;
    }
    return item;
  });
  
  
  this.setState({
    items : upState
  })
}

  
  render() {
    return (
      <div style={{width: "90%"}}>
        <Additem addTodo = {this.addTodo} />
        <List key="userslist" completeItem = { this.completeItem } deleteItem= {this.deleteItem} users = { this.state.items }  />
      </div>
    );
  }
}

export default App;
